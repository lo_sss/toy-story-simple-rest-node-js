# Toy Story Simple REST Node js

Used Technologies: TypeScript, Node.js, Express

# Features
* Add, update user
* Add, update toy
* Link toy to user
* Get sorted users by toysCount

# Instalation
1. Clone the repo
2. type $ npm install in main folder
3. create .env file in main project folder with content:

    PORT=3000
    <br />
    DB_PORT=5432
    <br />
    CONNECTIONSTRING=postgres://yzcpjofm:8Z1RxVMeNNks4fhKe1ZC1TCPRrxhQg4N@tai.db.elephantsql.com:5432/yzcpjofm

4. $ npm start to run project

# Sample calls:
* GET TOYS

    URL: http://localhost:3000/toys
    <br />
    body: /

* GET USERS

    URL: http://localhost:3000/users
    <br />
    body: /

* GET USER TOYS

    URL: http://localhost:3000/user-toys
    <br />
    body: /

* GET SORTED USERS

    URL: http://localhost:3000/users-toys-sorted
    <br />
    body: /

* POST TOY

    URL: http://localhost:3000/toys
    <br />
    body: {
    <br />
    "id": 32,
    <br />
    "name": "Test_Toy32",
    <br />
    "description": "Test_Toy32_description"
    <br />
    }

* POST USER

    URL: http://localhost:3000/users
    <br />
    body: {
    <br />
    "id": 40,
    <br />
    "name": "Simon"
    <br />
    }

* POST TOY TO USER

    URL: http://localhost:3000/user-toys
    <br />
    body: {
    <br />
    "userId": 21,
    <br />
    "toyId": 32
    <br />
    }

* PUT TOY

    URL: http://localhost:3000/toys
    <br />
    body: {
    <br />
    "id": 32,
    <br />
    "description": "new description"
    <br />
    }

* PUT USER

    URL: http://localhost:3000/users
    <br />
    body: {
    <br />
    "id": 40,
    <br />
    "name": "Maria"
    <br />
    }







