import { IToy, IUpdateToyObj } from "./../models/toy";
import { IData } from "./../models/user";
import { IAddToyObj } from "../models/toy";

export class ToysService {
  public toyExistCheck(body: IAddToyObj, allToys: IData): void {
    if (allToys.rows.find((toy: IToy) => toy.id === body.id)) {
      throw new Error(`Toy with id ${body.id} already exists!`);
    }
    if (allToys.rows.find((toy: IToy) => toy.name === body.name)) {
      throw new Error(`Toy with name ${body.name} already exists!`);
    }
  }

  public toyNotExistCheck(body: IUpdateToyObj, allToys: IData): void {
    if (!allToys.rows.find((toy: IToy) => toy.id === body.id)) {
      throw new Error(`Can not find toy with id ${body.id}!`);
    }
  }
}
