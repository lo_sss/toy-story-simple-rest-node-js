import {
  IUserObj,
  IUserToyObj,
  IData,
  IUser,
  IUserToys,
  IAddUserObj,
} from "./../models/user";
import { IToy } from "./../models/toy";

export class UserService {
  public userExistsCheck(body: IAddUserObj, allUsers: IData): void {
    if (allUsers.rows.find((user: IUser) => user.id === body.id)) {
      throw new Error(`User with id ${body.id} already exists!`);
    }
    if (allUsers.rows.find((user: IUser) => user.name === body.name)) {
      throw new Error(`User with name ${body.name} already exists!`);
    }
  }

  public userNotExistCheck(body: IAddUserObj, allUsers: IData): void {
    if (!allUsers.rows.find((user: IUser) => user.id === body.id)) {
      throw new Error(`Can not find user with id ${body.id}!`);
    }
  }

  public checkUserAndToy(
    body: IUserToyObj,
    allUsers: IData,
    allToys: IData,
    allUsersToys: IData
  ): void {
    if (!allUsers.rows.find((user: IUser) => user.id === body.userId)) {
      throw new Error(`Can not find user with id ${body.userId}!`);
    }
    if (!allToys.rows.find((toy: IToy) => toy.id === body.toyId)) {
      throw new Error(`Can not find toy with id ${body.toyId}!`);
    }
    if (
      allUsersToys.rows.some(
        (ownership: IUserToys) => ownership.toy_id === body.toyId
      )
    ) {
      throw new Error(`Toy with id ${body.toyId} already has an owner!`);
    }
  }

  public sortUsers(
    allUsers: IData,
    allToys: IData,
    allUsersToys: IData
  ): IUserObj[] {
    const usersWithToys = allUsers.rows.map((user: IUser) => {
      const userObj: IUserObj = {
        userName: user.name,
        toysCount: 0,
        toysNames: [],
      };

      allUsersToys.rows.forEach((ownership: IUserToys) => {
        if (user.id === ownership.user_id) {
          const foundToy: IToy = allToys.rows.find(
            (toy: IToy) => toy.id === ownership.toy_id
          );
          userObj.toysNames.push(foundToy.name);
        }
      });

      userObj.toysCount = userObj.toysNames.length;

      return userObj;
    });

    const sortedUsers = usersWithToys.sort(
      (x: IUserObj, y: IUserObj) => y.toysCount - x.toysCount
    );

    return sortedUsers;
  }
}
