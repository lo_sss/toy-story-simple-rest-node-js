export interface IToy {
  id: number;
  name: string;
  created: string;
  updated: string;
  description: string;
}

export interface IAddToyObj {
  id: number;
  name: string;
  description: string;
}

export interface IUpdateToyObj {
  id: number;
  description: string;
}
