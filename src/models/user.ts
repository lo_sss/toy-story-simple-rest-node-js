export interface IUser {
  id: number;
  name: string;
  created: string;
  updated: string;
}

export interface IUserToys {
  user_id: number;
  toy_id: number;
}

export interface IData {
  [key: string]: any;
}
export interface IUserObj {
  userName: string;
  toysCount: number;
  toysNames: string[];
}

export interface IAddUserObj {
  id: number;
  name: string;
}

export interface IUserToyObj {
  userId: number;
  toyId: number;
}
