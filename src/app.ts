require('dotenv').config();

import express, { Request, Response, NextFunction } from "express";
import bodyParser from "body-parser";
import { Database } from "./database/database";
import { UserService } from './services/usersService';
import { UsersController } from './controllers/users';
import { ToysController } from "./controllers/toys";
import { ToysService } from "./services/toysService";

const db = new Database();
db.connect();

const app = express();
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const toysService = new ToysService()
const usersService = new UserService();

new ToysController(toysService, app, db);
new UsersController(usersService, app, db);

app.listen(process.env.PORT);
