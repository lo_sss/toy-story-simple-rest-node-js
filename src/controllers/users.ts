import { Request, Response, NextFunction } from "express";
import { Database } from "./../database/database";
import { UserService } from "./../services/usersService";
export class UsersController {
  db: Database;
  app: any;
  userService: UserService;

  constructor(userService: UserService, app: any, db: Database) {
    this.userService = userService;
    this.db = db;
    this.app = app;
    this.userService = new UserService();
    this.initialize();
  }

  initialize() {
    this.app.get("/users", this.getUsers.bind(this));
    this.app.post("/users", this.addUser.bind(this));
    this.app.put("/users", this.updateUser.bind(this));
    this.app.get("/user-toys", this.getUsersToys.bind(this));
    this.app.post("/user-toys", this.addToyToUser.bind(this));
    this.app.get("/users-toys-sorted", this.getUsersSortedByToys.bind(this));
  }

  async getUsers(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await this.db.getAllUsers();
      res.status(200).send(result.rows);
    } catch (err) {
      res.status(503).send(err.message);
    }
  }

  async addUser(req: Request, res: Response, next: NextFunction) {
    try {
      const body = req.body;
      const allUsers = await this.db.getAllUsers();
      this.userService.userExistsCheck(body, allUsers);
      await this.db.addNewUser(body);
      res
        .status(200)
        .send({ message: `User with id ${body.id} successfully added!` });
    } catch (err) {
      res.status(503).send(err.message);
    }
  }

  async updateUser(req: Request, res: Response, next: NextFunction) {
    try {
      const body = req.body;
      const allUsers = await this.db.getAllUsers();
      this.userService.userNotExistCheck(body, allUsers);
      await this.db.updateUser(body);
      res
        .status(200)
        .send({ message: `User with id ${body.id} successfully updated!` });
    } catch (err) {
      res.status(503).send(err.message);
    }
  }

  async getUsersToys(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await this.db.getAllUsersToys();
      res.status(200).send(result.rows);
    } catch (err) {
      res.status(503).send(err.message);
    }
  }

  async addToyToUser(req: Request, res: Response, next: NextFunction) {
    try {
      const body = req.body;
      const allUsers = await this.db.getAllUsers();
      const allToys = await this.db.getAllToys();
      const allUsersToys = await this.db.getAllUsersToys();
      this.userService.checkUserAndToy(body, allUsers, allToys, allUsersToys);
      await this.db.addNewToyToUser(body);
      res
        .status(200)
        .send({
          message: `Toy with id ${body.toyId} successfully added to user with id ${body.userId}!`,
        });
    } catch (err) {
      res.status(503).send(err.message);
    }
  }

  async getUsersSortedByToys(req: Request, res: Response, next: NextFunction) {
    try {
      const allUsers = await this.db.getAllUsers();
      const allToys = await this.db.getAllToys();
      const allUsersToys = await this.db.getAllUsersToys();
      const sortedUsers = this.userService.sortUsers(
        allUsers,
        allToys,
        allUsersToys
      );
      res.status(200).send(sortedUsers);
    } catch (err) {
      res.status(503).send(err.message);
    }
  }
}
