import { Request, Response, NextFunction } from "express";
import { Database } from "./../database/database";
import { ToysService } from "../services/toysService";

export class ToysController {
  toysService: ToysService;
  db: Database;
  app: any;

  constructor(toysService: ToysService, app: any, db: Database) {
    this.toysService = toysService;
    this.db = db;
    this.app = app;
    this.initialize();
  }

  initialize() {
    this.app.get("/toys", this.getToys.bind(this));
    this.app.post("/toys", this.addToy.bind(this));
    this.app.put("/toys", this.updateToy.bind(this));
  }

  async getToys(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await this.db.getAllToys();
      res.status(200).send(result.rows);
    } catch (err) {
      res.status(503).send(err.message);
    }
  }

  async addToy(req: Request, res: Response, next: NextFunction) {
    try {
      const body = req.body;
      const allToys = await this.db.getAllToys();
      this.toysService.toyExistCheck(body, allToys);
      await this.db.addNewToy(body);
      res
        .status(200)
        .send({ message: `Toy with id ${body.id} successfully added!` });
    } catch (err) {
      res.status(503).send(err.message);
    }
  }

  async updateToy(req: Request, res: Response, next: NextFunction) {
    try {
      const body = req.body;
      const allToys = await this.db.getAllToys();
      this.toysService.toyNotExistCheck(body, allToys);
      await this.db.updateToy(body);
      res
        .status(200)
        .send({ message: `Toy with id ${body.id} successfully updated!` });
    } catch (err) {
      res.status(503).send(err.message);
    }
  }
}
