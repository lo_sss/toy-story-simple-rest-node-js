require("dotenv").config();

import { Client } from "pg";
import { IAddToyObj, IUpdateToyObj } from "./../models/toy";
import { IAddUserObj, IUserToyObj, IData } from "./../models/user";

export class Database {
  private conString = process.env.CONNECTIONSTRING;
  private client: Client;

  constructor() {
    this.client = new Client(this.conString);
  }

  connect() {
    this.client.connect(function (err) {
      if (err) {
        return console.error("could not connect to postgres", err);
      }
    });
  }

  async getAllUsersToys(): Promise<IData> {
    return await this.client.query("SELECT * FROM user_toys");
  }

  async getAllToys(): Promise<IData> {
    return await this.client.query("SELECT * FROM toys");
  }

  async getAllUsers(): Promise<IData> {
    return await this.client.query("SELECT * FROM users");
  }

  async addNewToy(toyObj: IAddToyObj): Promise<IData> {
    const { id, name, description = "None" } = toyObj;
    return await this.client.query(
      `INSERT INTO toys VALUES (${id}, '${name}', TO_TIMESTAMP(${Date.now()}), TO_TIMESTAMP(${Date.now()}), '${description}')`
    );
  }

  async updateToy(toyObj: IUpdateToyObj): Promise<IData> {
    const { id, description } = toyObj;
    return await this.client.query(
      `UPDATE toys SET description = ('${description}'), updated = (TO_TIMESTAMP(${Date.now()})) WHERE id = ${id}`
    );
  }

  async addNewUser(userObj: IAddUserObj): Promise<IData> {
    const { id, name } = userObj;
    return await this.client.query(
      `INSERT INTO users VALUES (${id}, '${name}', TO_TIMESTAMP(${Date.now()}), TO_TIMESTAMP(${Date.now()}))`
    );
  }

  async updateUser(userObj: IAddUserObj): Promise<IData> {
    const { id, name } = userObj;
    return await this.client.query(
      `UPDATE users SET name = ('${name}'), updated = (TO_TIMESTAMP(${Date.now()})) WHERE id = ${id}`
    );
  }

  async addNewToyToUser(userToyObj: IUserToyObj): Promise<IData> {
    const { userId, toyId } = userToyObj;
    return await this.client.query(
      `INSERT INTO user_toys VALUES (${userId}, ${toyId})`
    );
  }
}
